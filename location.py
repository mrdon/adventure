NORTH = "north"
SOUTH = "south"
WEST = "west"
EAST = "east"
DOWN = "down"
UP = "up"
EXITS = [NORTH, SOUTH, WEST, EAST, DOWN, UP]


class Location(object):
    name = ''
    description = ''
    ground = []
    exits = {}

    def __init__(self, name, description, ground=None, exits=None):
        self.name = name
        self.description = description
        if ground:
            self.ground = ground
        if exits:
            self.exits = exits

    def can_enter(self, inventory):
        return True


LOCATIONS = {}


def add_location(name, **kwargs):
    LOCATIONS[name] = Location(name=name, **kwargs)


add_location(name='Town Square', description='A town square with only five people.',
             exits={NORTH: "The blacksmith", SOUTH: "The swampy forest"}, ground=["Sword"])
add_location(name='The blacksmith', description='A blacksmithy for armor.',
             exits={SOUTH: "Town Square"})
add_location(name='The swampy forest', description='A swamp with lots of trees and water.',
             exits={NORTH: "Town Square", SOUTH: "A pool of water", EAST: "A dark cave"})
add_location(name='A dark cave', description="A cave so dark you can't see",
             exits={WEST: "The swampy forest"})
add_location(name='A pool of water', description="A big big pool of water.",
             exits={NORTH: "A swampy forest", DOWN: "A bunch of algae"})
add_location(name='A bunch of algae', description="More algae then you have ever seen in your life.",
             exits={UP: "The swampy forest"})
