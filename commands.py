from location import EXITS
from functools import partial
import cmd


class TextAdventureCmd(cmd.Cmd):

    def __init__(self, game):
        super(TextAdventureCmd, self).__init__() 
        self.game = game
        self.prompt = '\n> '

        for exit_name in EXITS:
            f = partial(self.handle_exit, exit_name)
            f.__doc__ = "Go %s" % exit_name
            setattr(self.__class__, 'do_' + exit_name, f)

    # The default() method is called when none of the other do_*() command methods match.
    def default(self, arg):
        print('I do not understand that command. Type "help" for a list of commands.')

    def handle_exit(self, exit_name, _):
        self.game.move_direction(exit_name)

    # A very simple "quit" command to terminate the program:
    def do_quit(self, _):
        """Quit the game."""
        return True

    def do_drop(self, arg):
        """"drop <item> - Drop an item from your inventory onto the ground."""
        self.game.drop(arg)

    def do_take(self, arg):
        """"take <item> - Take an item on the ground."""
        self.game.take(arg)

    def do_inventory(self, arg):
        """Display a list of the items in your possession."""
        self.game.display_inventory()

    do_inv = do_inventory