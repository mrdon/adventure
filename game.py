import textwrap
import commands
from location import LOCATIONS
from item import ITEMS
from collections import defaultdict

SCREEN_WIDTH = 80


class Game(object):
    location = LOCATIONS['Town Square']
    inventory = defaultdict(int)

    def display_location(self, location=None):
        """A helper function for displaying an area's description and exits."""
        # Print the room name.
        if location is None:
            location = self.location
        print(location.name)
        print('=' * len(location.name))

        # Print the room's description (using textwrap.wrap())
        print('\n'.join(textwrap.wrap(location.description, SCREEN_WIDTH)))

        # Print all the items on the ground.
        if len(location.ground) > 0:
            print()
            for item_name in location.ground:
                item = ITEMS[item_name]
                print(item.ground_description)

        # Print all the exits.
        print()
        for exit_name, exit_loc in location.exits.items():
            loc = LOCATIONS[exit_loc]
            if loc.can_enter(self.inventory):
                print('%s: %s' % (exit_name, exit_loc))

    def move_direction(self, direction):

        if direction in self.location.exits:
            print('You move to the %s.' % direction)
            next_loc = self.location.exits[direction]
            if next_loc.can_enter(self.inventory):
                self.location = LOCATIONS[next_loc]
                self.display_location(self.location)
                return

        print('You cannot move in that direction')

    def display_inventory(self):
        """Display a list of the items in your possession."""

        if len(self.inventory) == 0:
            print('Inventory:\n  (nothing)')
            return

        # get a list of inventory items with duplicates removed:
        print('Inventory:')
        for item_name, item_count in self.inventory.items():
            if item_count > 1:
                print('  %s (%s)' % (item_name, item_count))
            elif item_count:
                print('  ' + item_name)

    def take(self, item_word):
        item_word = item_word.lower()

        if item_word == '':
            print('Take what? Type "look" the items on the ground here.')
            return

        items = [ITEMS[item] for item in self.location.ground] 
        cant_take = False
        for item in items:
            if not item.takeable:
                cant_take = True
                continue
            print('You take %s.' % item.short_description)
            self.location.ground.remove(item.name)
            self.inventory[item.name] += 1
            return

        if cant_take:
            print('You cannot take "%s".' % item_word)
        else:
            print('That is not on the ground.')

    def drop(self, item_word):
        item_word = item_word.lower()

        items = [ITEMS[item_name] for item_name, count in self.inventory.items() if count]

        try:
            item = next(item for item in items if item_word in item.words)
        except StopIteration:
            print('You do not have "%s" in your inventory.' % item_word)
            return

        print('You drop %s.' % item.short_description)
        self.inventory[item.name] -= 1
        if self.inventory[item.name] == 0:
            del self.inventory[item.name]
        self.location.ground.append(item.name)

if __name__ == '__main__':
    print('Text Adventure Demo!')
    print('====================')
    print()
    print('(Type "help" for commands.)')
    print()
    game = Game()
    game.display_location()

    commands.TextAdventureCmd(game).cmdloop()
    print('Thanks for playing!')