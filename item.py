class Item(object):
    def __init__(self, name='', editable=False, takeable=False, ground_description='', short_description='',
                 long_description='', words=None):
        if words is None:
            words = []
        self.name = name
        self.editable = editable
        self.takeable = takeable
        self.ground_description = ground_description
        self.short_description = short_description
        self.long_description = long_description
        self.words = words


ITEMS = {}


def add_item(name, **kwargs):
    ITEMS[name] = Item(name=name, **kwargs)


add_item(name='Sword', ground_description='a small sword', short_description='sword',
         long_description='A small sword with a sharp point', takeable=True, words=['sword'])

# ITEMS = {
# 'Sword' : Weapon(attack=3, name='Sword', words=['sword'], group_description='a small sword',
# 		short_description='sword', long_description='A small sword with a sharp point'),

# }
add_item(name='torch', ground_description='A torch with a flame', short_description='A torch',
         long_description='A torch with a burning flame', takeable=True, words=['torch'])